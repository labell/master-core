﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class OrderMsg : IJsonSerializable
    {
        private int msg_type;
        private long func_call_id;
        private int fc_source;
        private bool order_kind;
        private Order order;

        internal OrderMsg(int msg_type, long func_call_id, int fc_source, bool order_kind, Order order) //конструктор сообщения #1
        {
            this.msg_type = msg_type;
            this.func_call_id = func_call_id;
            this.fc_source = fc_source;
            this.order_kind = order_kind;
            this.order = order;
        }

        internal OrderMsg(int msg_type, bool order_kind, Order order) //конструктор сообщения #2
        {
            this.msg_type = msg_type;
            func_call_id = -1L;
            fc_source = -1;
            this.order_kind = order_kind;
            this.order = order;
        }

        public string Serialize()
        {
            if (fc_source != -1)
            {
                return JsonManager.FormTechJson(msg_type, func_call_id, fc_source, order_kind, order);
            }
            else
            {
                return JsonManager.FormTechJson(msg_type, order_kind, order);
            }
        }
    }
}
